/**
 * @file
 * @author Denise Ratasich
 * @date 30.10.2014
 *
 * ROS node receiving commands from the AVR and interprets it to
 * control the parrot, i.e., sends appropriate cmd_vel data to the
 * ardrone_driver.
 *
 * parameters:
 *	~port: serial port, e.g., /dev/ttyUSB0 (default).
 *
 * publish:
 *	/cmd_vel [geometry_msgs/Twist]
 */

// general includes
#include <iostream>
#include <boost/thread.hpp>
#include <termios.h>
#include <unistd.h>

// ROS includes
#include "ros/ros.h"
#include "ros/console.h"
#include "std_msgs/Empty.h"
#include "geometry_msgs/Twist.h"

// project specific includes
#include "ASIOSerialDevice.h"
#include "avrcommands.h"

// --- ROS ----------------------------------------------------- /
ros::Publisher pub_cmdvel;
ros::Publisher pub_takeoff;
ros::Publisher pub_land;
// ------------------------------------------------------------- /

// --- SERIAL -------------------------------------------------- /
/** Handles the serial port (connection to AVR). */
static ASIOSerialDevice serialDevice;
/** Set when an alive message has been sent until the AVR responds
 * with an ack. */
static bool waitForAck = 0;

/** The command format. */
struct command
{
  char type;
  //uint16_t data;
};
// ------------------------------------------------------------- /

// --- KEYBOARD ------------------------------------------------ /
class MutexedKey 
{
  boost::mutex mtx_;	// for mutual exclusion (of read/write)
  char key_;		// the data to guard: key_, consumed_
  bool consumed_;
public:
  MutexedKey() {
    consumed_ = true;
  }
  void setKey(char key) {
    mtx_.lock();
    key_ = key;
    consumed_ = false;
    mtx_.unlock();
  }
  char getKey(void) {
    char key;
    mtx_.lock();
    key = key_;
    consumed_ = true;
    mtx_.unlock();
    return key;
  }
  bool isKeyAvailable(void) {
    bool available;
    mtx_.lock();
    available = !consumed_;
    mtx_.unlock();
    return available;
  }
};

MutexedKey mtxKey;
// ------------------------------------------------------------- /

/**
 * Interprets the command and sends appropriate messages to the
 * ardrone_driver.
 *
 * @param cmd The pointer to the structure holding the command to
 * apply.
 */
static void apply_command(struct command * const cmd)
{
  std_msgs::Empty doThat;
  geometry_msgs::Twist hover;
  hover.linear.x = hover.linear.y = hover.linear.z = 0;
  hover.angular.x = hover.angular.y = hover.angular.z = 0;

  switch(cmd->type) {
  case CMD_TAKE_OFF:
    pub_takeoff.publish(doThat);
    ROS_INFO("take off");
    break;
  case CMD_LAND:
    pub_land.publish(doThat);
    ROS_INFO("land");
    break;
  case CMD_HOVER:
    pub_cmdvel.publish(hover);
    ROS_INFO("hover");
    break;
  case CMD_LEFT:
    hover.linear.y = 0.2;
    pub_cmdvel.publish(hover);
    ROS_INFO("left");
    break;
  case CMD_RIGHT:
    hover.linear.y = -0.2;
    pub_cmdvel.publish(hover);
    ROS_INFO("right");
    break;
  case CMD_FORWARD:
    hover.linear.x = 0.2;
    pub_cmdvel.publish(hover);
    ROS_INFO("forward");
    break;
  case CMD_BACKWARD:
    hover.linear.x = -0.2;
    pub_cmdvel.publish(hover);
    ROS_INFO("backward");
    break;
  case CMD_UP:
    hover.linear.z = 0.5;
    pub_cmdvel.publish(hover);
    ROS_INFO("up");
    break;
  case CMD_DOWN:
    hover.linear.z = -0.5;
    pub_cmdvel.publish(hover);
    ROS_INFO("down");
    break;
  case CMD_TURN_LEFT:
    hover.angular.z = -0.1;
    //pub_cmdvel.publish(hover);
    ROS_INFO("turn left");
    break;
  case CMD_TURN_RIGHT:
    hover.angular.z = 0.1;
    //pub_cmdvel.publish(hover);
    ROS_INFO("turn right");
    break;
  case CMD_PUSH_FORWARD:
    hover.linear.x = 1;
    pub_cmdvel.publish(hover);
    ROS_INFO("forward");
    break;
  default:
    ROS_ERROR("unknown command");
    break;
  }

  ROS_DEBUG("command %c applied", cmd->type);
}

/**
 * Callback from serial device, when a character has been received.
 *
 * @param data Pointer to the bytes received.
 * @param len The number of bytes received.
 */
static void serial_data_received(const unsigned char* data, size_t len)
{
  static enum msg curMessage = NONE;
  static struct command curCmd;
  //static unsigned char message[MAX_LENGTH];
  static int charCnt = 0;
  
  // get byte for byte from data
  for(int i = 0; i < len; i++) 
  {
    // next byte received
    unsigned char rec = data[i];

    std::cout << rec;
    //printf("%c", rec);
    //fflush(stdout);

    switch(curMessage) {
    case CMD:
      if (rec == END_CHARACTER) {
	std::cout << '\n';
	curMessage = NONE;

	// if it's an ack from the AVR, don't respond
	if (curCmd.type == CMD_ACK) {
	  waitForAck = 0;
	  ROS_DEBUG("alive");
	} else {
	  // all other commands have to be applied ...  
	  apply_command(&curCmd);

	  // ... and responded with an ack
	  ROS_DEBUG("send ack");
	  std::vector<unsigned char> msg;
	  msg.push_back(START_CHARACTER);
	  msg.push_back(CMD_ACK);
	  msg.push_back(END_CHARACTER);
	  serialDevice.Write(msg);
	}
      } else {
	// received a byte between start and end byte
	curCmd.type = rec;
      }
      break;
    case NONE:
      if (rec == START_CHARACTER) {
	// initiates a command
	curMessage = CMD;
      } else {
	// debug message, so simply print it
	//std::cout << rec;
      }
      break;
    }
  }
}

static void applyParams(void)
{
  ros::NodeHandle nh("~");
  
  // get params
  std::string port = "/dev/ttyUSB0"; 
  if (nh.hasParam("port")) {
    nh.getParam("port", port);
    ROS_INFO_STREAM("use serial port '" << port << "'");
  } else {
    ROS_INFO_STREAM("use default serial port '" << port << "'");
  }

  // configure serial port
  serialDevice.Open(port, 19200);
  serialDevice.SetReadCallback(serial_data_received);
}

/** 
 * Thread function to read from keyboard. 
 */
void listen_to_keyboard(void)
{
  char key;
  bool run = 1;

  while(run) {
    key = getchar();	// blocking!
    mtxKey.setKey(key);

    if (key == 'q') {
      run = 0;		// leave loop, terminates this thread
    }
  }
}

/**
 * Main entry point.
 */
int main(int argc, char **argv)
{
  int aliveCnt = 0;
  std_msgs::Empty doThat;
  geometry_msgs::Twist hover;
  hover.linear.x = hover.linear.y = hover.linear.z = 0;
  hover.angular.x = hover.angular.y = hover.angular.z = 0;


  // ROS initialization
  ros::init(argc, argv, "parrot_imu_control");
  ros::NodeHandle nh("~");
  applyParams();
  pub_cmdvel = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
  pub_takeoff = nh.advertise<std_msgs::Empty>("/ardrone/takeoff", 10);
  pub_land = nh.advertise<std_msgs::Empty>("/ardrone/land", 10);
  ros::Rate loop_rate(100);
  ROS_INFO("initialization done");

  // start receiving from the microcontroller
  serialDevice.Start();

  // disable buffering and echo of terminal
  struct termios newt;
  tcgetattr(STDIN_FILENO, &newt);		// get params of the current terminal
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);	// change attributes immediately

  // start thread to read from keyboard (updates mtxKey when key
  // pressed)
  boost::thread keyboardThread(listen_to_keyboard);

  while(ros::ok())
  {
    // send alive every 1s
    // aliveCnt++;
    // if (aliveSendCnt >= 10) {
    //   aliveCnt = 0;
    //   waitForAlive = 1;
      
    //   ROS_DEBUG("send alive request...");
    //   std::vector<unsigned char> msg;
    //   msg.push_back('?');
    //   serialDevice.Write(msg);
    // } else if (aliveCnt >= 1  &&  waitForAlive) {
    //   // still no alive (after 100ms)
    //   ROS_ERROR("no alive from AVR\a");      
    // }
    
    // check if key pressed
    if (mtxKey.isKeyAvailable()) {
      char key = mtxKey.getKey();
      if (key == 'q')		// regular shutdown with 'q'
	break;

      if (key == 'l')
	pub_land.publish(doThat);
      if (key == 'h')
	pub_cmdvel.publish(hover);
 
      // send key to AVR (as command)
      std::vector<unsigned char> msg;
      msg.push_back(key);
      serialDevice.Write(msg);
      
      /*
      if (key == 'l') {
	pub_land.publish(doThat);
      } else if (key == 'h') {
	pub_cmdvel.publish(hover);
      } else {
	// send key to AVR (as command)
	std::vector<unsigned char> msg;
	msg.push_back(START_CHARACTER);
	msg.push_back(key);
	msg.push_back(END_CHARACTER);
	serialDevice.Write(msg);
      }
      */
    }
    
    ros::spinOnce();		// check received ROS messages
    loop_rate.sleep();		// wait a period of 100ms
  }

  // Following lines will be executed when the node is terminated with
  // a signal (e.g., Ctrl-C -> ros::ok() returns 0). However, ROS
  // things won't work any more (e.g., ROS_INFO), because the ROS node
  // is already unconnected from the master?!

  // enable buffering and echo again
  newt.c_lflag |= (ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);

  ROS_INFO("Shut down.");

  return 0;
}
