/**
 * @file
 * @author Denise Ratasich
 * @date 01.11.2014
 *
 * Commands from the AVR. From AVR firmware: drone.c.
 */

#ifndef __AVRCOMMANDS_H__
#define __AVRCOMMANDS_H__

/** 
 * Message types of the AVR. 
 *
 * Commands have to be interpreted. All other messages/characters
 * should only be printed for debugging, no special treatment.
 */
enum msg {CMD, NONE};

#define START_CHARACTER		'S'
#define END_CHARACTER		'E'

// from AVR
#define CMD_TAKE_OFF		't'
#define CMD_LAND		'l'
#define CMD_HOVER		'h'
#define CMD_LEFT		'a'
#define CMD_RIGHT		'b'
#define CMD_FORWARD		'c'
#define CMD_BACKWARD		'd'
#define CMD_UP			'e'
#define CMD_DOWN		'f'
#define CMD_TURN_LEFT		'g'
#define CMD_TURN_RIGHT		'i'
#define CMD_PUSH_FORWARD	'j'

// from/to AVR
#define CMD_ACK			'z'

// to AVR (only cmd, without start and end)
#define CMD_ALIVE		'?'
#define CMD_SHOW_HELP		'h'
#define CMD_DEBUG_IMU		'1'
#define CMD_DEBUG_ADC		'2'
#define CMD_DEBUG_CTRL		'3'

#endif
